# Adnosco for Chrome
Adnosco automatically saves all the forms that you complete and submit during your browsing. It can then restore them as you wish when you have to re-fill those forms.

## Getting started
You need to have NodeJS installed on your computer. Install the dependencies and build the project:
```
    npm install && bower install && gulp
```
In chrome, go to `chrome://extensions` and load the *src* folder  as an unpacked extension.
When it's done, you can view the background process by inspecting the background page in the extension's infos at the same address.

Additionnaly, you can install the [ForerunnerDB extension](https://chrome.google.com/webstore/detail/forerunnerdb-explorer/gkgnafoehgghdeimbkaeeodnhbegfldm) to explore the content of the local database that is storing the forms when you inspect the background page (a new tab is available when opening the inspector tools).

## Deploy
If you want to package the extension in order to have a .crx file that you can distribute to alpha testers for example, then follow the instructions in the link below:
[https://chrome.google.com/webstore/developer/dashboard](https://chrome.google.com/webstore/developer/dashboard).

If you want to deploy the extension on the Chrome Web Store for all users then follow this link:
[https://chrome.google.com/webstore/developer/dashboard](https://chrome.google.com/webstore/developer/dashboard)



