var gulp = require('gulp');
var vulcanize = require('gulp-vulcanize');
var crisper = require('gulp-crisper');

// Minifies + separates the js from the html because of the Content Security Policy
gulp.task('vulcanize', function () {
  return gulp.src('elements.html')
    .pipe(vulcanize())
    .pipe(crisper())
    .pipe(gulp.dest('src/lib/elements'));
});

gulp.task('copy', function () {
  return gulp.src([
    'node_modules/forerunnerdb/js/dist/fdb-all.min.js'
    ])
    .pipe(gulp.dest('src/lib'));
});

gulp.task('default', ['vulcanize', 'copy']);


