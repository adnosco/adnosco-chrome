/**
 * This script is injected in each page the user visits.
 * Performs queries to the DOM to find the forms and listens for data changes.
 */

// Aggregate of all the fields found in the page
var form = {
    hash: 0,
    url: window.location.hostname + window.location.pathname, // Discard the protocol and parameters of the URL
    host: window.location.hostname,
    fields: []
};
findFields();

/**
 * Get all the forms in the document and add listeners to these forms inputs
 */
function findFields() {
    var elements = document.querySelectorAll("input, select, textarea");
    console.log("found " + elements.length + " fields");

    elements.forEach(function (element) {
        var field = createField(element);
        if (!!field) {
            form.fields.push(field);
        }
    });

    // Now that the fields are added, we can calculate the hash
    form.hash = calculateHash();
}

/**
 * Called whenever the user leave the page (close, reload, navigate)
 * Send the collected data to be persisted to the parent extension 
 */
window.onbeforeunload = function () {
    submitForm();
};

chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        switch (request.type) {
            case Msg.GET_CURRENT_FORM:
                updateForm(); // Update form before sending
                sendResponse({ status: "ok", form: form });
                break;
            case Msg.PREVIEW_FORM:
                restoreForm(request.form, true);
                break;
            case Msg.RESET_FORM:
                resetForm();
                break;
            case Msg.RESTORE_FORM:
                restoreForm(request.form, false);
                break;
        }
    }
);

function submitForm() {
    form.date = new Date();

    form.completed = 0;
    for (var i = 0; i < form.fields.length; i++) {
        var field = form.fields[i];
        updateField(field);
        form.completed += hasBeenSet(field) ? 1 : 0;
    }
    if (form.completed > 0) { // Only send the form if at least one field has been completed
        chrome.runtime.sendMessage(null, form);
    }
}

function restoreForm(form, highlight) {
    form.fields.forEach(function (field) {
        var el = restoreElement(field, highlight);
        if (!highlight) {
            removeHighlight(el);
        }
    })
}

function resetForm() {
    form.fields.forEach(function (field) {
        var element = restoreElement(field, false);
        if (!!element) {
            removeHighlight(element);
        }
    })
}

function updateForm() {
    form.fields.forEach(function (field) {
        updateField(field);
    })
}

function calculateHash() {
    // Sort the fields to get a consistent hash
    sortFormFieldsByName();
    var fullString = form.url;
    form.fields.forEach(function (field) {
        fullString += field.name + field.type + field.id;
    });
    return hash(fullString);
}

function hash(chain) {
    var hash = 0;
    if (chain.length == 0) return hash;
    for (i = 0; i < chain.length; i++) {
        char = chain.charCodeAt(i);
        hash = ((hash << 5) - hash) + char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}

/**
 * Sort form's fields by name
 */
function sortFormFieldsByName() {
    form.fields.sort(function (a, b) {
        return a.name.localeCompare(b.name)
    });
}