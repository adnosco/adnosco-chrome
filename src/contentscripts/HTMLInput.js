//  Map that store the reference of the corresponding HTML element of a Field object, if it has been found 
var fieldElementMap = new Map();

/**
 * Create a Field object from an HTML element listening for element changes
 * @param element {HTMLElement}
 * @returns {Field}
 */
function createField(element) {
    var field = {
        name: element.name,
        xpath: getXPath(element),
        type: element.type,
        id: element.id,
    };
    fieldElementMap.set(field, element);

    if (element.type == "submit"
        || element.type == "button"
        || element.type == "reset"
        || element.type == "image"
        || element.type == "password") {
        return undefined;
    }
    else {
        updateField(field);
        element.addEventListener("focusout", function (event) {
            updateField(field)
        });
        return field;
    }
}

/**
 * Update the field value from the current corresponding HTML element value
 * @param field {Field}
 */
function updateField(field) {
    var element = getElementFromField(field);
    if (field.type == "checkbox" || field.type == "radio") {
        if (field.defaultValue == undefined) { // Set the default value if it hasnt been done
            field.defaultValue = element.checked;
        }
        field.checked = element.checked;
    }
    else {
        if (field.defaultValue == undefined) { // Set the default value if it hasnt been done
            field.defaultValue = element.value.trim();
        }
        field.value = element.value;
    }
}

/**
 * Checks if a field has been completed by the user
 * @returns completed {Boolean}
 */
function hasBeenSet(field) {
    if (field.type == "checkbox" || field.type == "radio") {
        return field.checked !== field.defaultValue;
    }
    else if(field.type == "select-one") {
        return field.value !== field.defaultValue;
    }
    else {
        return field.value.trim() !== "";
    }
}

/**
 * Restore the corresponding HTML element of a field object
 * @param field {Field} 
 * @returns element {HTMLElement} 
 */
function restoreElement(field, highlight) {
    var element = getElementFromField(field);
    if (element === undefined) {
        console.log("Could not find element to restore");
        return undefined;
    }

    if (element.type !== field.type) {
        console.log("Attempted to restore input, but target and source type do not match");
        return undefined;
    }
    if (element.type == "checkbox" || element.type == "radio") {
        element.checked = field.checked;
    }
    else {
        // Don't apply theming if the value to be restored is empty or if it's the default one
        if (field.value.trim() != "" && element.value != field.value && highlight) {
            addHighlight(element);
        }
        element.value = field.value;
    }
    return element;
}

function addHighlight(element) {
    element.classList.add("adnosco-autocomplete");
}

function removeHighlight(element) {
    element.classList.remove("adnosco-autocomplete");
}

/**
 * Return the element that matches either the field id, the field type and name or its path
 * @returns {Input} HTML input
 */
function getElementFromField(field) {
    var element = fieldElementMap.get(field);
    if (!!element) { // Element already stored in the map
        return element;
    }

    if (field.id != null && field.id != "") {
        element = document.getElementById(field.id);
    }
    else {
        var query = "";
        if (field.name != null && field.name != "") {
            query += "[name='" + field.name + "']";
        }
        if (field.type != null && field.type != "") {
            query += "[type='" + field.type + "']";
        }

        var queryRes = document.querySelectorAll(query);

        if (queryRes.length > 1 || queryRes.length == 0) { // Multiple elements or none found, use xpath instead
            element = document.evaluate(field.xpath, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
        } else {
            element = queryRes[0];
        }
    }
    return element;
}

/**
 * Get the path of a node
 * Example: /HTML[1]/BODY[1]/FORM[2]/INPUT[9]
 * @param {Node} node
 * @returns {String} Xpath
 */
function getXPath(node) {
    var comp, comps = [];
    var parent = null;
    var xpath = '';
    var getPos = function (node) {
        var position = 1,
            curNode;
        if (node.nodeType == Node.ATTRIBUTE_NODE) {
            return null;
        }
        for (curNode = node.previousSibling; curNode; curNode = curNode.previousSibling) {
            if (curNode.nodeName == node.nodeName) {
                ++position;
            }
        }
        return position;
    }

    if (node instanceof Document) {
        return '/';
    }

    for (; node && !(node instanceof Document); node = node.nodeType == Node.ATTRIBUTE_NODE ? node.ownerElement : node.parentNode) {
        comp = comps[comps.length] = {};
        switch (node.nodeType) {
            case Node.TEXT_NODE:
                comp.name = 'text()';
                break;
            case Node.ATTRIBUTE_NODE:
                comp.name = '@' + node.nodeName;
                break;
            case Node.PROCESSING_INSTRUCTION_NODE:
                comp.name = 'processing-instruction()';
                break;
            case Node.COMMENT_NODE:
                comp.name = 'comment()';
                break;
            case Node.ELEMENT_NODE:
                comp.name = node.nodeName;
                break;
        }
        comp.position = getPos(node);
    }

    for (var i = comps.length - 1; i >= 0; i--) {
        comp = comps[i];
        xpath += '/' + comp.name;
        if (comp.position != null) {
            xpath += '[' + comp.position + ']';
        }
    }

    return xpath;

}