const InputType = {
    CHECKBOX: "checkbox",
    TEXT: "text",
    PASSWORD: "password",
    SUBMIT: "submit",
    RADIO: "radio",
    NUMBER: "number"
}

var fdb = new ForerunnerDB(),
    db = fdb.db('Adnosco-DB');
forms = db.collection('forms'),
    blacklist = db.collection('blacklist');

forms.load(function (err, tableStats, metaStats) {
    if (err) {
        console.log("load failed" + err);
    }
});

blacklist.load(function (err, tableStats, metaStats) {
    if (err) {
        console.log("load failed" + err);
    }
});

/**
 * Persist a form in the db
 * @param {Form} form
 * @returns {Boolean} success
 */
function saveForm(form) {
    if (form.fields.length > 0) {
        form.date = db.make(new Date());
        forms.insert(form);
        forms.save(function (err) {
            return err;
        });
    }
}

function db_getFormsByUrl(url) {
    return forms.find({
        url: { $eq: url }
    });
}

function db_getFormsByHash(hash) {
    return forms.find({
        hash: { $eq: hash }
    });
}

function db_getFormsByHashAndMask(hash, fields) {
    var res = db_getFormsByHash(hash);

    res.forEach(function (form) {
        form.score = 0;
        form.isMatchingMask = true;
        for (var i = 0; i < fields.length; i++) {
            var fieldMatching = false;
            for (var j = 0; j < form.fields.length; j++) {
                fieldMatching = isMatching(fields[i], form.fields[j])
                if (fieldMatching) {
                    break;
                }
            }
            if (!fieldMatching) {
                form.isMatchingMask = false;
                break;
            }
        }
    });

    res.sort(function (b, a) {
        return a.score - b.score;
    });

    var matchingMask = res.filter(function (e) { return e.isMatchingMask });
    var others = res.filter(function (e) { return !e.isMatchingMask });

    return { matchingMask: matchingMask, others: others };
}

function db_isHostBlacklisted(hostname) {
    return blacklist.find({
        hostname: { $eq: hostname }
    }).length > 0;
}

function db_addHostToBL(hostname) {
    if (!db_isHostBlacklisted(hostname)) {
        blacklist.insert({
            hostname: hostname
        });
    }
}

function db_removeHostFromBL(hostname) {
    blacklist.remove({
        hostname: { $eq: hostname }
    });
}

function calculateMatchingScore(field, db_field, length) {
    var score = 0;
    if (db_field.value != "" && field.value != "") {
        switch (field.type) {
            case InputType.RADIO:
            case InputType.CHECKBOX:
                if (field.checked == db_field.checked)
                    score += 2;
                break;
            default:
                if (field.type == InputType.TEXT)
                    try {
                        var exp = new RegExp(field.value.toLowerCase() + '[^]*');
                        if (db_field.value.toLowerCase().match(exp)) {
                            score += (2 * length + db_field.value.length);
                            console.log(db_field.value + " matchs " + exp + " score: " + score);
                        }
                    } catch (error) {
                        console.error(error + " " + field + "score " + score);
                    }
        }
    } else if (db_field.value != "") {
        score = 1;
    }
    return score;
}

function isMatching(field, db_field) {
    if (field.type == db_field.type
        && field.id == db_field.id
        && field.name == db_field.name) {

        if (field.type == "checkbox" || field.type == "radio") {
            return field.checked == db_field.checked;
        }
        else {
            if (field.value.trim() == "") {
                return true;
            }
            else {
                try {
                    var exp = new RegExp(field.value.toLowerCase() + '[^]*');
                    return db_field.value.toLowerCase().match(exp)
                } catch (error) {
                    console.error(error + " " + field + "score " + score);
                    return false
                }
            }
        }
    }
    else {
        return false;
    }

}

/**
 * Not working...
 */
function db_getFormsByHashAndMask2(hash, matchingFields) {
    var values = [];
    var types = [];

    for (var i = 0; i < matchingFields.length; i++) {
        var e = matchingFields[i];
        values.push(new RegExp(e.value));
        types.push(e.type);
    }
    var arraySavedForm = forms.find({
        hash: { $eq: hash },
    }, {
            $elemsMatch: {
                fields: {
                    value: { $in: values },
                    type: { $in: types }
                }
            }

        });
    return arraySavedForm;
}
