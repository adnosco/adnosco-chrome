/**
 * Messages exchanged between content scripts and the background page
 */
const Msg = {
    GET_CURRENT_FORM: 0,
    PREVIEW_FORM: 1,
    RESET_FORM: 2,
    RESTORE_FORM: 3
}