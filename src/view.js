var hostnameEl;
var toggleSaveEl;
var toggleHideDuplicatesEl;
var selectSortMethodEl;
var formsListEl;
var disabledTextEl;

document.addEventListener('DOMContentLoaded', function () {
    hostnameEl = document.getElementById("hostname");
    toggleSaveEl = document.getElementById("toggle-save");
    toggleHideDuplicatesEl = document.getElementById('toggle-hide-duplicates');
    selectSortMethodEl = document.getElementById("select-sort-method");
    formsListEl = document.getElementById("forms-result");
    disabledTextEl = document.getElementById("disabled-text");

    setHostname();
    toggleHideDuplicatesEl.addEventListener('iron-change', onClickHideDuplicates);
    toggleSaveEl.addEventListener('iron-change', onClickToggleSave);
    selectSortMethodEl.addEventListener('iron-select', onClickSortMethod);
});

var background = chrome.extension.getBackgroundPage();

var restored = false; // Prevents resetForm() from being called after confirmRestoreForm()

var matchingForms;
var otherForms;

background.isHostBlacklisted(function (isBlacklisted) {
    shouldSaveForms(!isBlacklisted)
});

function setHostname() {
    background.getCurrentTab(function (tab) {
        var hostname = background.getLocation(tab.url).hostname;
        if (!!hostname) {
            hostnameEl.innerText = hostname;
        }
    })
}

function shouldSaveForms(shouldSave) {
    if (shouldSave) {
        toggleSaveEl.setAttribute("checked", "");
        selectSortMethodEl.removeAttribute("disabled");
        toggleHideDuplicatesEl.removeAttribute("disabled");
        formsListEl.style.display = "block";
        disabledTextEl.style.display = "none";
    }
    else {
        selectSortMethodEl.setAttribute("disabled", "");
        toggleHideDuplicatesEl.setAttribute("disabled", "");
        formsListEl.style.display = "none";
        disabledTextEl.style.display = "block";
    }
}

background.getSavedForms(function (result) {
    var matchingCount = result.matchingMask.length;
    var otherCount = result.others.length;
    displayText(matchingCount, otherCount);
    matchingForms = result.matchingMask;
    otherForms = result.others;

    sortByDate();
})

function displayList(forms, listId) {
    var list = document.getElementById(listId);
    list.innerHTML = ''; // Remove all elements from the list
    if(toggleHideDuplicatesEl.checked) {
        forms = getFormsWithoutDuplicates(forms);
    }
    forms.forEach(function (form) {
        addForm(list, form);
    });
}

function addForm(list, form) {
    var item = document.createElement("paper-item");
    var text = document.createTextNode(getFormLabel(form));
    item.appendChild(text);
    item.addEventListener('click', function (e) { // Restore form
        background.restoreForm(form);
        restored = true;
        window.close();
    });
    item.addEventListener('mouseenter', function (e) { // Preview form
        background.previewForm(form);
    });
    item.addEventListener('mouseleave', function (e) { // Remove preview
        if (!restored) background.resetForm();
    });
    item.addEventListener('focusin', function (e) { // Preview form
        background.previewForm(form);
    });
    item.addEventListener('focusout', function (e) { // Remove preview
        if (!restored) background.resetForm();
    });
    list.appendChild(item);
}

function onClickSortMethod(e) {
    if (e.detail.item.id == "sortby-date") {
        sortByDate();
    }
    else if (e.detail.item.id == "sortby-completion") {
        sortByCompletion();
    }
}

function onClickHideDuplicates(event) {
    displayList(matchingForms, "matching-forms");
    displayList(otherForms, "other-forms");
}

function onClickToggleSave(event) {
    shouldSaveForms(event.target.checked);
    background.setHostBlacklisted(!event.target.checked);
}

function sortByDate() {
    matchingForms.sort(function (a, b) {
        return new Date(b.date) - new Date(a.date);
    });
    otherForms.sort(function (a, b) {
        return new Date(b.date) - new Date(a.date);
    });
    document.getElementById("sort-label").innerText = "Date";
    displayList(matchingForms, "matching-forms");
    displayList(otherForms, "other-forms");
}

function sortByCompletion() {
    matchingForms.sort(function (a, b) {
        return b.completed - a.completed;
    });
    otherForms.sort(function (a, b) {
        return b.completed - a.completed;
    });
    document.getElementById("sort-label").innerText = "Completion";
    displayList(matchingForms, "matching-forms");
    displayList(otherForms, "other-forms");
}

/**
 * Text labelling a form
 * @returns {String} label
 */
function getFormLabel(form) {
    var date = new Date(form.date);
    var completedText = form.completed > 1 ? " (" + form.completed + " fields)" : " (one field)";
    return date.toLocaleString() + completedText;
}

function displayText(matchingCount, othersCount) {
    var matchingText;
    if (matchingCount > 1) {
        matchingText = matchingCount + " forms are matching your current inputs:"
    } else if (matchingCount == 1) {
        matchingText = "One form is matching your current inputs:"
    }
    else {
        matchingText = "No matching form found."
    }
    document.getElementById('matching-text').appendChild(document.createTextNode(matchingText));

    var otherText;
    if (othersCount > 1) {
        otherText = othersCount + " forms are not matching your current inputs:"
    } else if (matchingCount == 1) {
        otherText = "One form is not matching your current inputs:"
    } else {
        otherText = "";
    }
    document.getElementById('others-text').appendChild(document.createTextNode(otherText));
}

/**
 * Calculate a unique Id per form,
 * then sort using a map:
 *      - the map is a map<Id, form>
 *      - Insert the Id and form on a map
 *      - return the map.values array
 */
function getFormsWithoutDuplicates(forms) {
    var formsMap = new Map();
    for (var index = 0; index < forms.length; index++) {
        var form = forms[index];
        var ident = "";
        for (var i = 0; i < form.fields.length; i++) {
            var field = form.fields[i];
            try {
                ident += field.value.toLowerCase();
            } catch (error) { }
        }
        formsMap.set(ident, index);
        console.log(ident + " -> " + formsMap.get(ident));
    }
    var res = [];
    for (var [key, value] of formsMap) {
        console.log(key + " = " + value);
        res.push(forms[value])
    }
    return res;
}

/**
 * Not used - Experimental
 */
function takeScreenshot() {
    chrome.tabs.setZoom(null, 0.3, function (e) {
        // To take the whole form, first we change the current tab zoom's 
        console.log(e);
        chrome.tabs.captureVisibleTab(null, {}, function (image) {
            // You can add that image HTML5 canvas, or Element.
            var div = document.getElementById('displayed_img');
            console.log(image);
            var img = document.createElement("img");
            img.src = image;
            div.appendChild(img);
            // Reset the zoom to default
            chrome.tabs.setZoom(null, 0, null);
        });
    });
}