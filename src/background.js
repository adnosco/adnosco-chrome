/**
 * The extension background script
 */

/**
 * Listens to the messages sent by the content scripts.
 */
chrome.runtime.onMessage.addListener(function (form, sender, sendResponse) {
    isHostBlacklisted(function (isBL) {
        if (!isBL) {
            saveForm(form);
        }
    })
});

function getFormsMatchingMask(form) {
    var fields = form.fields;
    return db_getFormsByHashAndMask(form.hash, fields);
}

function getSavedForms(callback) {
    sendMessageToContentScript({ type: Msg.GET_CURRENT_FORM }, function (response) {

        var forms = getFormsMatchingMask(response.form);
        callback(forms);
    });
}

function isHostBlacklisted(callback) {
    getCurrentTab(function (tab) {
        var loc = getLocation(tab.url);
        if (!!loc) {
            var isBL = db_isHostBlacklisted(loc.hostname)
            callback(isBL)
        }
        else {
            callback(false)
        }
    })
}

function setHostBlacklisted(isBlacklisted) {
    getCurrentTab(function (tab) {
        var loc = getLocation(tab.url);
        if (!!loc) {
            if (isBlacklisted) {
                db_addHostToBL(loc.hostname);
            }
            else {
                db_removeHostFromBL(loc.hostname);
            }
        }
    })
}

function previewForm(form) {
    sendMessageToContentScript({ type: Msg.PREVIEW_FORM, form: form });
}

function restoreForm(form) {
    sendMessageToContentScript({ type: Msg.RESTORE_FORM, form: form });
}

function resetForm() {
    sendMessageToContentScript({ type: Msg.RESET_FORM });
}

function sendMessageToContentScript(msg, callback) {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, msg, function (response) {
            if (!!callback) {
                callback(response);
            }
        });
    });
}

function getCurrentTab(callback) {
    chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        callback(tabs[0]);
    });
}

function getLocation(url) {
    var match = url.match(/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)([\/]{0,1}[^?#]*)(\?[^#]*|)(#.*|)$/);
    return match && {
        protocol: match[1],
        host: match[2],
        hostname: match[3],
        port: match[4],
        pathname: match[5],
        search: match[6],
        hash: match[7]
    }
}
